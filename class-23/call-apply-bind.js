// Implicit/Auto context
// Right now, we cannot change the context of a function that it's running in

function printFullNameDRY(emailDomain, country) {
    console.log(this)
    console.log(`The email is ${this.firstName}.${this.lastName}@${emailDomain}.${country}`)
    console.log(this.firstName + " " + this.lastName)
}

// const a = () => {
//     this.sas
// } 

const student1 = {
    firstName: "Kumar",
    lastName: "Swamy",
    // printFullName: function() {
    //     console.log(this.firstName + " " + this.lastName)
    // }
}

const student2 = {
    firstName: "Sanath",
    lastName: "Menon",
    // printFullName: function() {
    //     console.log(this.firstName + " " + this.lastName)
    // }
}

// student1.printFullName()
// student2.printFullName()


// printFullNameDRY()


// Explicit context
// Where we can manually set a context to a function
// So that the this keyword inside the function
// POints to the same context

// Call method

// Call method does two things
// 1. Call the method
// 2. Sets the context to be the first parameter of the call function

// Set the context of my function to be student1

// Syntax of call function
// function.call(context, paramater1, parameter2....)
// printFullNameDRY.call(student1, "gmail", "in")


// printFullNameDRY.call(student2, "yahoo", 'com')


//Apply method


// Syntax of apply function
// Apply method takes in a single argument for the function paramters
// function.apply(context, [paramater1, parameter2....])

// printFullNameDRY.apply(student1, ["gmail", "in"])



// Bind method

// For both call & apply, the function is run imemediately
// Now if I want to set the context but I want to reuse
// The function later, then I have to use bind

// By using bind
// We are manually setting a context to a function
// And returning it so that we can call it later 
// Without any object.func() calls

const printForLater = printFullNameDRY.bind(student1, "gmail", "in")

printForLater()


// DO NOT USE this INSIDE OF ARROW FUNCTIONS









// Write a DRY way to find out if an array has an even length

// 1.Normal function
// function checkIfLengthEven(array) {
//     return array.length % 2 == 0
// }
// checkIfLengthEven([1, 2, 3, 4])

// 2. Using explicit context i.e call, appl,. bind
// function checkIfLengthEven(){
//     return this.length % 2 == 0
// }
// console.log(checkIfLengthEven.call([1, 2, 3, 4, 5]))

// 3. Use Array prototype
Array.prototype.checkIfLengthEven = function() {

    console.log(this[0])
    return this.length % 2 == 0
}

console.log([1, 2, 3, 4, 5].checkIfLengthEven())