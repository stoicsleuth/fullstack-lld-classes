// Primitive type

let s1 = "Hello"

let s2 = "World"

console.log(s1, s2)

s1 = s2

console.log(s1, s2)


// Reference Types

// const obj1 = {
//     name: "Object 1",
//     prop: "Test"
// }

// const obj2 = obj1

// console.log(obj1, obj2)

// obj2.prop = "Non-test"

// console.log(obj1, obj2)



// Two ways of copying a reference datatype

// Shallow copy
// We can copy an object, but only the first level

const obj1 = {
    name: "Object 1",
    prop: "Test",
    inner: {
        innerProp: "Test"
    }
}

// const obj2 = {...obj1}

// console.log(obj1, obj2)

// obj2.inner.innerProp = "New Test"

// console.log(obj1, obj2)


// Deep copy

// You sholuld not use it if your object contains functions

// 1. stringify my object
const objIntoString = JSON.stringify(obj1)

// 1. parse the string back into a new object
const obj2 = JSON.parse(objIntoString)

console.log(obj1, obj2)

obj2.inner.innerProp = "New Test"

console.log(obj1, obj2)


class Dog {
    constructor(name) {
        this.name = name
        this.blameString = " ate my homework"
    }

    blame() {
        function fn () {
            return this.blameString
        }
        return fn.call(this)
    }
}


const myDog = new Dog('My Dog')
myDog.name = "Your dog"

console.log(myDog.name + myDog.blame())
