import React, { useState } from 'react'

function Login() {
    const [userName , setUserName] = useState('')
    const [password , setPassword] = useState('')
    const [age, setAge] = useState(20)

    const handleLogin = async (e)=>{
     try {
        e.preventDefault()
        console.log(userName, password)
     } catch (error) {
        
     }
    }
    return (
        <div>
            <form>
                <label style={{ margin: '20px' }}>UserName</label>
                <input type='text' placeholder='UserName 1' value={userName} onChange={(e)=> setUserName(e.target.value)} />
                <label style={{ margin: '20px' }}>Password</label>
                <input type='password' placeholder='Password' value={password} onChange={(e)=> setPassword(e.target.value)} />


                <label style={{ margin: '20px' }}>Age</label>
                <input placeholder='Age' value={age} onChange={(e)=> setAge(e.target.value)} />


                <button style={{
                    margin: '20px'
                }} disabled={!userName || !password} onClick={handleLogin}>Login</button>

            </form>
        </div>
    )
}

export default Login