import { render, screen } from '@testing-library/react';
import Login from './Login';


test('ensure that the inputs and the buttons exist on the screen', () => {
    render(<Login />)
    const userNameElem = screen.getByPlaceholderText(/username/i)
    const passworkElem = screen.getByPlaceholderText(/password/i)
    const buttonElem = screen.getByRole("button")

    expect(userNameElem).toBeInTheDocument()
    expect(passworkElem).toBeInTheDocument()
    expect(buttonElem).toBeInTheDocument()
});

test('ensure the initial state of the inputs', () => {
    render(<Login />)
    const userNameElem = screen.getByPlaceholderText(/username/i)
    const passworkElem = screen.getByPlaceholderText(/password/i)
    const ageElem = screen.getByPlaceholderText(/age/i)
    const buttonElem = screen.getByRole("button")

    expect(userNameElem.value).toBe('')
    expect(passworkElem.value).toBe('')
    expect(ageElem.value).toBe('20')
    expect(buttonElem).toBeDisabled()
})






