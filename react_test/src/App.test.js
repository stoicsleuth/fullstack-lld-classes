import { render, screen } from '@testing-library/react';
import App from './App';

// The entire file is a test-file

// This is a test-case
test('renders learn react link', () => {
  // This is where you write the code for your tests
  // And see if the output is correct.
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);

  // Think of this as a comparison
  //Expect(a) means that a is being compared
  expect(linkElement).to
  expect(linkElement).toBeInTheDocument();
});

test('Check for four list items', () => {
    render(<App />);
    const listElem = screen.getAllByRole("listitem")
    expect(listElem).toHaveLength(4)
})








