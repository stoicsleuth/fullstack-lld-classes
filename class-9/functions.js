// Ways to define a function

// 1: Function keyword
function sum(a, b) {
  return a + b
}

const a = sum(4, 5)
console.log(a)

// Function expression

// Anonymous function
var sum2 = function (a,b) {
  return a +b 
}

var subtract = function (a,b) {
  return a - b 
}
console.log(sum2(1, 2))
console.log(sum2(1, 5))
console.log(subtract(5, 1))


// 3 -> Arrow functions
// By itself, arrow functions don't have any name
// Arrow functions must always be assigned to a variable
// Arrow functions are anonymous
const sum3 = (a, b) => {
  return a + b 
}
console.log(sum3(3, 4))

// const square = (a) => {
//   return a * a
// }
// If I have a single return statement, I can omit body and directly write the value of teh return
const square = a => a * a

console.log(square(3))


// Functions in JS are first -class citizen, which means we can pass in functions as parameters to other function

function callme(fn) {
  console.log(fn())
}


function sum10(a, b) {
  console.log(a+b)
}

callme(() => 6)