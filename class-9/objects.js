const groot = {
  name: 'Groot',
  age: 999,
  planet: {
    name: 'Groot',
    solarSystem: {
      name: 'Groot'
    }
  },
  sayHi: function () {
    console.log('I am Groot')
  }
}

console.log(groot.planet.solarSystem.name)

// groot.sayHi()