console.log('Array!')

//Recap

let a = [1, 2, 3]
// new Array()

//remove last element
a.pop()
console.log(a)

//Remove first element
a.shift()
console.log(a)

//Push an item to end
a.push(10)
console.log(a)

// //Pushing to the start
// a.unshift(100)
// console.log(a)


// //Nested/2D ARRAY
// a.push([1, 2, 3])
// console.log(a[3][1])


// // Push can take more than one elements -> it adds all to the end of the array
// a.push(1, 2, 3)

// console.log(a)

// ext_func(arr[0], arr[1]...)
//Destructuring
// Spread operator, takes an array and turns that into a list of parameters
// Mostly used when passing an array/object in a function
a.push(...[1, 2, 3])
// a.push(1, 2, 3)
console.log(a)


// A property of the array - not a function
const c = a.length
console.log(c)

const d = a.push(42)
console.log(d)

// let b = [3, 4, 5]
// console.log(...b[0] )





