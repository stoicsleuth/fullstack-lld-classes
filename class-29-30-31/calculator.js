function sum (a, b) {
    return a + b 
}

function sub (a, b) {
    return a - b 
}

function mul (a, b) {
    return a * b 
}

function div (a, b) {
    return a / b 
}

const CONST_OBJECT = {
    mykey: "myvalue"
}


// This is a way of exporting certain functions/0bjects from your file
module.exports = {
    addition: sum,
    subtraction: sub,
    multiplication: mul,
    division: div,
    CONST_OBJECT: CONST_OBJECT
}