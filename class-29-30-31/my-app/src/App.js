import Counter from "./components/Counter";
import MyComponent from "./components/MyComponent";
import MyComponent2 from "./components/MyComponent2";
import NewsHeadline from "./components/NewsHeadline";
import Products from "./components/Products";
import MyForm from "./components/MyForm";
import MyFormSmarter from "./components/MyFormSmarter"
import ExampleEffect from "./components/ExampleEffect";

function App() {
  return (
    <div className="App">
      {/* <MyComponent />
      <MyComponent2 /> */}

      {/* Props -> properties that we pass onto a component to use dynamic values */}
      {/* <NewsHeadline headline="This is my custom headline" time="1d ago" />
      <NewsHeadline headline="React is easy" time="2d ago" /> */}
    {/* <Counter initialValue={10} />
    <Counter initialValue={20} />
    <Counter initialValue={30} /> */}
    {/* <Products /> */}
    {/* <MyForm /> */}
    <ExampleEffect />
    </div>
  );
}

export default App;
