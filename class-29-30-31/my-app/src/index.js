import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// This is JSX, which is basically writing HTML along with JS
// <App /> -> React.createElement("div", { className: "app"}, "Hi from React")
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function

