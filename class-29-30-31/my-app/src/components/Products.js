import React from 'react'

function Products() {
    const products = [{
        id: "001",
        value: "Snacks",
        
    }, {
        id: "002",
        value: "Coke"
    }, {
        id: "003",
        value: "Pizza"
    }]

    return (
        <div>
            <div>Products</div>
            <ol>
                {
                    products.map((product) => {
                        return (
                            <li key={product.id}>{product.value}</li>
                        )
                    })
                }
            </ol>
        </div>
    )
}

export default Products