import React, { useState } from 'react'

function MyFormSmarter() {
    const [formState, setFormState] = useState({
        address: '',
        firstName: '',
        lastName: '',
    })

    const handleFormChange = function (e) {
        const {name, value} = e.target
        // Spread operator, used to create a new object
        const newFormState = {...formState}
        newFormState[name] = value

        setFormState(newFormState)

    }


    // const handleFirstNameChange = function(e) {
    //     setFirstName(e.target.value)
    // }

    // const handleLastNameChange = function(e) {
    //     setLastName(e.target.value)
    // }


    const handleFormSubmit = function(e) {
        // API Call to make with my form values
        e.preventDefault()

        console.log({ firstName: formState.firstName, address: formState.address})

        // console.log("Form is submitted with values", firstName, lastName)

    }

  return (
    <form>
        <div>MyForm</div>
        <label>
            Enter first name
            <input name="firstName" onChange={handleFormChange} type="text" value={formState.firstName} />
        </label>
        <label>
            Enter last name
            <input name="lastName" onChange={handleFormChange} type="text" value={formState.lastName} />
        </label>

        <label>
            Address
            <input name="address" onChange={handleFormChange} type="text" value={formState.address} />
        </label>

        <button onClick={handleFormSubmit} type="submit">Submit</button>
    </form>
  )
}

export default MyFormSmarter