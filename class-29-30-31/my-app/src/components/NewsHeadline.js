import React from 'react'

// function myFunc() {
//     return "value"
// }

// const myFunc = () => "value"

// props object is immutable -> cannot modify that value directly
function NewsHeadline({ headline, time }) {
    return (
        <div>
            <h4>{headline}</h4>
            <p>{time}</p>
        </div>
    )
}

function testFn(str) {
    const ownStr = "New"

    return ownStr + str
}


export default NewsHeadline