import React, { useEffect, useState } from 'react'

function ExampleEffect({ initialValue }) {
    // React hooks are basically reusable JS function
    // which are used in react components
    // Two types -> inbuilt
    const [counter, setCounter] = useState(initialValue || 0)
    const [counter2, setCounter2] = useState(initialValue || 0)


    useEffect(() => {
        console.log("Running this function with new value of counter")
    }, [counter])

    useEffect(() => {
        console.log("Running this function again with new value of counter")
    }, [counter])

    useEffect(() => {
        console.log("Running this function with new value of counter2")
    }, [counter2])

    const increaseCount = (event) => {
        setCounter(counter + 1)
    }

    const increaseCount2 = (event) => {
        setCounter2(counter2 + 1)
    }

    const decreaseCount = () => {
        setCounter(counter - 1)
    }

    return (
        <div>
            <div>Value: {counter}</div>
            <button onClick={increaseCount}>Increment</button>
            <button onClick={decreaseCount} >Decrement</button>


            <div>Value: {counter2}</div>
            <button onClick={increaseCount2}>Increment</button>
            <button onClick={decreaseCount} >Decrement</button>
        </div>
    )
}

export default ExampleEffect