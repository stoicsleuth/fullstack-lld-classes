function MyComponent2 () {
    const myvar = 10

    const returnOne = () => 1


    // This is not actually HTML
    // This is called JSX (Javascript XML)
    // HTML + JS, which means you can use JS inside of HTML
    // Just by using curly braces
    return (
        <h1>Value is {myvar} {10+20} {returnOne()}</h1>
    )
}

// Two ways to export a component
// If I want to export ony a single thing from my file -> default export
// If I want to export multiple thing -> named exports 

export default MyComponent2