function MyComponent () {
    // any JS in the function
    // My function must contain only one return that returns HTML
    return (
        <h1>Returning H1 tag</h1>
    )
}

// Two ways to export a component
// If I want to export ony a single thing from my file -> default export
// If I want to export multiple thing -> named exports 

export default MyComponent