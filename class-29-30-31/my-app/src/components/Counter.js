import React, { useState } from 'react'

function Counter({ initialValue }) {
    // React hooks are basically reusable JS function
    // which are used in react components
    // Two types -> inbuilt
    const [counter, setCounter] = useState(initialValue)

    const increaseCount = (event) => {
        console.log(event)
        setCounter(counter + 1)
    }

    const decreaseCount = () => {
        setCounter(counter - 1)
    }

    return (
        <div>
            <div>Value: {counter}</div>
            <button onClick={increaseCount}>Increment</button>
            <button onClick={decreaseCount} >Decrement</button>
        </div>
    )
}

export default Counter