import React, { useState } from 'react'

function MyForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')


    const handleFirstNameChange = function(e) {
        setFirstName(e.target.value)
    }

    const handleLastNameChange = function(e) {
        setLastName(e.target.value)
    }


    const handleFormSubmit = function(e) {
        // API Call to make with my form values
        e.preventDefault()

        console.log("Form is submitted with values", firstName, lastName)

    }

  return (
    <form>
        <div>MyForm</div>
        <label>
            Enter first name
            <input onChange={handleFirstNameChange} type="text" value={firstName} />
        </label>
        <label>
            Enter last name
            <input onChange={handleLastNameChange} type="text" value={lastName} />
        </label>

        <button onClick={handleFormSubmit} type="submit">Submit</button>
    </form>
  )
}

export default MyForm