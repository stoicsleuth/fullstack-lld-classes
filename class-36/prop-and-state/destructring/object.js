const initialObject =  {
    a: 'b',
    b: 'c'
}

const finalObject = {
    ...initialObject,
    newKey: "abc"
}