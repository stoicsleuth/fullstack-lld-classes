import { BrowserRouter, Routes, Route } from "react-router-dom";
import Family from "./components/Family";
import FamilyContext from "./components/FamilyContext";
import Outsider from "./components/Outsider";
import Factorial from "./components/Factorial";
import Ref from "./components/Ref";
// Discuss Loader here

function App() {

  // const infoForFamily = {
  //   name: "The Godfathers",
  //   infoForFamily: {
  //     name: "God Parents"
  //   },
  //   infoForGrandSon: {
  //     name: "Info: Grand Son"
  //   }
  // }

  return (
    <BrowserRouter>

    <div className="App">
      {/* <FamilyContext.Provider value={infoForFamily}>
        <Family />
      </FamilyContext.Provider>
      <Outsider /> */}
      <Ref />
    </div>
    </BrowserRouter>
  );
}

export default App;
