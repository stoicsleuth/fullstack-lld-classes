import React, { useRef, useState } from 'react'

function Ref() {
  const [testState, setTestState] = useState(0)
  const [name, setName] = useState("")

  const valueRef = useRef(40)
  const inputRef = useRef()


  let plainVariable = 100

  console.log({ plainVariable })

  const increaseState = () => {
    valueRef.current = valueRef.current + 1
    setTestState(testState + 1)
  }

  const changeColor = () => {
    inputRef.current.style.color = "blue"
    inputRef.current.focus()
  }

  return (
    <div>
        <input ref={inputRef} style={{ border: 'red' }} type="text" value={name} onChange={(e) => setName(e.target.value)} />
        
        <button onClick={changeColor}>Change color of Input</button>

        <button onClick={increaseState}>Increase</button>
        <span>{testState} / {plainVariable} / {valueRef.current} </span>
    </div>
  )
}

export default Ref