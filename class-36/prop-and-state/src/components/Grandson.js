import React, { useContext } from 'react'
import FamilyContext from './FamilyContext'

function Grandson() {
  const data = useContext(FamilyContext)

  return (
    <div className='gson'>
        {data.infoForGrandSon.name}
        Grandson
    </div>
  )
}

export default Grandson