import React, { useContext } from 'react'
import Parent from './Parent'
import FamilyContext from './FamilyContext'

function Family() {
    const data = useContext(FamilyContext)

  return (
    <div className='family'>
        {data.infoForFamily.name}
        <div>Family</div>
        <Parent />
    </div>
  )
}

export default Family