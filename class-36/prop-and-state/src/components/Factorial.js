import React, { useMemo, useState } from 'react'


function factorialOF(n){
    console.log('factorial function called')

    return n<=0 ? 1 : n * factorialOF(n-1)
}

// Memoization -> storing value of an expensive calculation
// So that it can be used later on for the same input

export default function Factorial() {
  const [number, setNumber] = useState(1)

  console.log("number", number)

  const memoizedValue = useMemo(() => factorialOF(number), [number])

//   const value = factorialOF(number)

  const onChange = (e) => {
    setNumber(Number(e.target.value))
  }

  return (
    <div>
        <span> Factorial of: </span> <input value={number} onChange={onChange} type="number" />
        <span>Factorial value: {memoizedValue}</span>
    </div>
  )
}
