import React from 'react'
import Son from './Son'

function Parent() {
  return (
    <div className='parent'>
        <div>Parent</div>
        <Son />
    </div>
  )
}

export default Parent