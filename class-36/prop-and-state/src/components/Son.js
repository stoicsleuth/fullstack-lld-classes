import React from 'react'
import Grandson from './Grandson'
import Granddaughter from './Granddaughter'

function Son() {
  return (
    <div className='children'>
        <div>Son</div>
        <Grandson />
        <Granddaughter />
    </div>
  )
}

export default Son