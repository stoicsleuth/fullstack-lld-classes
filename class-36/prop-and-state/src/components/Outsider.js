import React, { useContext } from 'react'
import FamilyContext from './FamilyContext'

function Outsider() {
  const data = useContext(FamilyContext)

  console.log({data})

  return (
    <div>Outsider</div>
  )
}

export default Outsider