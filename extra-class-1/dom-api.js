// // Selecting 

// Select by an ID -> getElementById("id") -> Returns a single element
// Select by a class -> getElementsByClassName("class") -> Return all elements with the className

// General CSS Selector -> querySelector("#myid") -> Single element
// General CSS Selector All -> querySelectorAll(".class") -> Returns all the elements that matches the CSS query


// // Once we get a list of items, we can loop over them by using
// // a normal for loop


// // Change 

// // How do we change the text of some DOM node
// selectedNode.innerText = "next text"

// // How do we change the HTML of some DOM node

// selectedNode.innerHTML = "<ol><li>One List Item</li></ol>"

// // Adding & Creating

// // document.createElement("p")

// appendChild -> append element 2 to element 1
// elemn1.appendChild(p)

// //Remove

// // Two ways to remove an element

// 1. remove() -> call on any element to remove it from DOM -> permanently


// 2. display = "none" -> Removes it from visible screen, but it's present in DOM


