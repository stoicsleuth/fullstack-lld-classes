const myPromise = () => Promise.resolve('I have resolved!');
const myPromise2 = () => Promise.resolve('I have resolved 2!');

function firstFunction() {
    myPromise().then(res => console.log(res)); // pushed yo mcq
    console.log('one thing'); // printed immediately 
}


// Whenever we use a function with the async keyword, it will NOT
// get executed immediately, and get pushed to mcq
// When this function gets pushed to mcq
// I already have one callback
// res => console.log(res) |  secondFunction
async function secondFunction() {
    const val = await myPromise2();
    console.log(val)
    console.log('one more thing');
}

firstFunction();
secondFunction();