// async - await deals with only promises not other async stuff
// Syntactic sugar on top of promises


// Async - await will make asynchronous code look synchronous

function test() {
    Promise.resolve("First value")
        .then((val) => val + "once more")
        .then((val) => console.log(val))
        .catch((e) => console.log(e))
}

async function testAsync() {
    try {
        const val = await Promise.resolve("First value")
        const val2 =  val + "once more"
        console.log(val2)
    } catch (error) {
        console.log(e)
    }
}

test()