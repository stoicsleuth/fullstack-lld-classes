// I have bunch of contacts
const contacts = [{
    name: "Amit",
    age: 10,
    customProp: false
}, {
    name: "Ramit",
    age: 20
}, {
    name: "Kamit",
    age: 30
}, {
    name: "Jamit",
    age: 40
}]

function checkProp(contactsArray, name, prop) {
    let customPropFound = false
    let nameFound = false

    contactsArray.forEach((contact) => {
        if(contact.name === name && contact.hasOwnProperty(prop)) {
            console.log(contact[prop])
            nameFound = true
            customPropFound = true
        }
    })

    return  {
        customPropFound : customPropFound,
        nameFound : nameFound
    }

}

console.log(checkProp(contacts, "Amit", "customProp"))