// Sync example

// function task1() {
//     console.log('Performing task 1')
// }

// function tasks2() {
//     console.log('Performing task 2')
// }

// task1()
// tasks2()

// Simulate a function taking a long time

// function task1() {
//     console.log('Performing task 1')
// }

// function heavyTaskSync(){
//     const time = Date.now()
//     // const i = 0

//     // Trick to wait sequentially for some time
//     while(Date.now() - time < 3000){

//     }
//     // while(i < 10000000){
//     //     i++
//     // }
//     console.log('Perfomred heavy task')
// }

// function tasks2() {
//     console.log('Performing task 2')
// }

// task1()
// heavyTaskSync() // This is a blocking behavior for task2
// tasks2()

// Async 


function task1() {
    console.log('Performing task 1')
}

function heavytaskAsync() {
    setTimeout(() => {
        console.log("Performed heavy task")
    }, 3000)
}

function tasks2() {
    console.log('Performing task 2')
}

task1()
heavytaskAsync() // This is non-blocking behavior of async 
tasks2()