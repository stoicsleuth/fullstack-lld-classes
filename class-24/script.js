const dataArray = [
    { name: 'Adam', age: 23},
    { name: 'Steve', age: 25},
]

function getData(){
    // setTimeout(() => {
        output = ''

        dataArray.forEach((data) => {            
            output += `<li>${data.name}</li>`
        })

        document.body.innerHTML += output
    // }, 3000)
}


function createNewData(newData, cb) {
    setTimeout(() => {
        dataArray.push(newData)
        cb()
    }, 3000)
}

function printHelloToBody() {
    document.body.innerHTML = "Hello World"
}

// What did I want to achieve?
// I wanted to fetch new data to my system, and display
// All in the page

// 1st step -> I am fetching new data from our network

createNewData({name: 'Dave', age: 30}, getData)
printHelloToBody()
// getData()



// This will display my data as a list in my page
// This is the perfect example where I can say that
// getData is dependent on createNewData
// getData()