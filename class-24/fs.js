// fs is basically file-system module
// Require is a keyword using which you import internal/external modules
const fs = require("fs")


function calculatePrime() {
    console.log('Calculating prime')
}
console.log("before")

// const data1 = fs.readFileSync("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file1.txt")
// console.log(data1, 'File 1')

// const data2 = fs.readFileSync("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file2.txt")
// console.log(data2, 'File 2')

// const data3 = fs.readFileSync("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file3.txt")
// console.log(data3, 'File 3')


// fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file1.txt", (err, data) => {
//     if(err) {
//         console.log("Ooops, that didn't work")
//     } else {
//         console.log('file 1')
//     }
// })

// fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file2.txt", (err, data) => {
//     if(err) {
//         console.log("Ooops, that didn't work")
//     } else {
//         console.log('file 2')
//     }
// })

// fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file3.txt", (err, data) => {
//     if(err) {
//         console.log("Ooops, that didn't work")
//     } else {
//         console.log('file 3')
//     }
// })


// calculatePrime()



fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file1.txt", (err, data) => {
    if(err) {
        console.log("Ooops, that didn't work")
    } else {
        console.log('file 1')

        fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file2.txt", (err, data) => {
            if(err) {
                console.log("Ooops, that didn't work")
            } else {
                console.log('file 2')


                fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file3.txt", (err, data) => {
                    if(err) {
                        console.log("Ooops, that didn't work")
                    } else {
                        console.log('file 3')
                    }


                    fs.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-24/file4.txt", (err, data) => {
                        if(err) {
                            console.log("Ooops, that didn't work")
                        } else {
                            console.log('file 3')
                        }
                    })
                })

            }
        })
    }
})
calculatePrime()

console.log("After")
