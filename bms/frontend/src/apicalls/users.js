import { BASEURL } from "./booking";

const { axiosInstance } = require("./axiosinstance");

// Regsiter a new User

export const RegisterUser = async (payload)=>{
    try {
        const response = await axiosInstance().post(`https://${BASEURL}/api/users/register"`, payload)
        return response
    } catch (error) {
        return error
    }
}


export const LoginUser = async (payload)=>{
    try {
        const response = await axiosInstance().post(`https://${BASEURL}/api/users/login`, payload)
        return response
    } catch (error) {
        return error
    }
}

//get Current User
export const GetCurrentUser = async () => {
    try {
        const response = await axiosInstance().get(`https://${BASEURL}/api/users/get-current-user`);
        return response.data
    } catch (error) {
        return error
    }
}

// Usage of URL params
// export const MockParm = async(id) => {
//     const response = await axiosInstance().post(`BASEURL/api/movies?movieId=${id}`, payload)
// }

