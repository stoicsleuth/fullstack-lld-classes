import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
// Try to import pages/Admin.js
// Try to import Admin/index.js
import Admin from './pages/Admin';
import Profile from './pages/Profile';
import TheatresForMovie from './pages/TheatresForMovie';


import "./stylesheets/alignments.css"
import "./stylesheets/custom.css"
import "./stylesheets/form-elements.css"
import "./stylesheets/sizes.css"
import "./stylesheets/theme.css"
import ProtectedRoute from './components/ProtectedRoute';
import Bookshow from './pages/Bookshow';

function App() {
  return (
    <div className="App">
      <p>BMS Application</p>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<ProtectedRoute> <Home /></ProtectedRoute>} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/admin" element={<ProtectedRoute><Admin /></ProtectedRoute>} />
          <Route path="/profile" element={<ProtectedRoute><Profile /></ProtectedRoute>} />
          <Route path="/book-show/:showId" element={<ProtectedRoute><Bookshow /></ProtectedRoute>} />
          <Route path="/movie/:id" element={<ProtectedRoute><TheatresForMovie /></ProtectedRoute>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
