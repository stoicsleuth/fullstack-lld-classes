const express = require("express")
var cors = require('cors')

require("dotenv").config()

const dbConfig = require("./config/dbConfig")


const userRoute = require("./routes/userRoute")
const movieRoute = require("./routes/movieRoute")
const theatreRoute = require("./routes/theatreRoute")
const showRoute = require("./routes/showRoute")
const bookingRoute = require("./routes/bookingRoute")

const app = express()
app.use(express.json())
app.use(cors())

app.use(express.static("build"))
app.use("/api/users", userRoute)
app.use("/api/movies", movieRoute)
app.use("/api/theatres", theatreRoute)
app.use("/api/shows", showRoute)
app.use("/api/booking", bookingRoute)

// Redirect Reac pages to homepage
app.get('/profile', function (req, res) {
    res.redirect('/');
});


// Common ports are -> (for a server)
// 5000, 8000, 8080
// Frontend -> 3000, 5431
app.listen(8080, () => {
    console.log("Server has started!")
})