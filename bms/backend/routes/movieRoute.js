const router = require("express").Router()
const authGuard = require("../middleware/authMiddleware")
const Movie = require("../models/movieModel")

router.post("/add-movie",authGuard, async (req, res) => {
    try {
        const newMovie = new Movie(req.body)
        await newMovie.save()

        res.status(200).send({
            success: true,
            message: "Movie is added!"
        })
    } catch (error) {
        console.log(error)
        res.status(500).send({
            success: false,
            message: "There was some error in adding movie!"
        })
    }
})

router.get("/list", authGuard, async (req, res) => {
    try {
        const movies = await Movie.find()

        res.status(200).send({
            success: true,
            message: "Movies fetched",
            movies
        })
    } catch (error) {
        res.status(500).send({
            success: false,
            message: "There was some issue in fetching movies.",
        })
    }
})

router.get("/get-by-id/:movieId", authGuard, async (req, res) => {
    try {

        const movie = await Movie.findById(req.params.movieId)

        res.status(200).send({
            success: true,
            message: "Movie details fetched",
            movie
        })
    } catch (error) {
        res.status(500).send({
            success: false,
            message: "There was some issue in fetching movie.",
        })
    }
})


router.post("/update-movie", authGuard,  async (req, res) => {
    try {
        await Movie.findByIdAndUpdate(req.body.movieId, req.body)

        res.send({
            success: true,
            message: "Movie updated with latest info!"
        })
    } catch (error) {
        res.send({
            success: false,
            error: "Something went wrong!"
        })
    }
})

router.post("/delete-movie", authGuard, async (req, res) => {
    try {
        await Movie.findByIdAndDelete(req.body.movieId)
        res.send({
            success: true,
            message: "Movie deleted!"
        })
    } catch (error) {
        res.send({
            success: false,
            message: "Some issue!"
        })
    }
})


module.exports = router