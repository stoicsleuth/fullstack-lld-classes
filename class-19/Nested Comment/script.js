const commentsContainer = document.querySelector('.comment-container')

const createInputBox = () => {
    // Create the div

    const div = document.createElement('div')
    div.setAttribute('class', "comment-reply-section")

    div.innerHTML = `
    <input class="input" type="text" value="" placeholder="Write your comment">
                    <button class="btn submit">Submit</button>`


    return div
}


const createAllCommentsSection = (value) => {
    // Create the div

    const div = document.createElement('div')
    div.setAttribute('class', "all-comments")

    div.innerHTML = `
    <div class="card">
    <span class="text">${value}</span>
    <span id="reply" class="reply">Add Reply</span>
</div>`


    return div
}


commentsContainer.addEventListener('click', (e) => {
    const replyButtonClicked = e.target.classList.contains('reply')
    const submitButtonClicked = e.target.classList.contains('submit')

    const closestAllCommentsSection = e.target.closest('.all-comments')


    if(replyButtonClicked) {
        // First step: add the new reply section to my closest all-comments section
        
        // Optional enhancement
        // Before adding this child, destroy/delete all the current reply sections on screen

        // Get all the reply sections on screen
        const allReplySections = document.querySelectorAll('.comment-reply-section')

        allReplySections.forEach((replySection) => {
            replySection.remove()
        })

        closestAllCommentsSection.appendChild(createInputBox())

    }

    else if (submitButtonClicked) {
        // Get the value of my input
        // Get to my nearest all-comments section
        // Add another all-comments section as a child
        // Hide the reply section
        const closestReplyParent = e.target.closest('.comment-reply-section')

        const inputComponent = closestReplyParent.children[0]
        const inputValue = inputComponent.value

        if (inputValue != "") {
            closestAllCommentsSection.appendChild(createAllCommentsSection(inputValue))
            closestReplyParent.remove()
        }
    }
})