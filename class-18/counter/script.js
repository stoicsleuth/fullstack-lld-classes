const textToChange = document.querySelector('#result')
const variable = document.querySelector('#variable')

const addButton = document.querySelector('#add')
const removeBtn = document.querySelector('#remove')
const resetBtn = document.querySelector('#reset')

let incrementByNumber = 0

const LOCALSTORAGE_KEY = 'counter'

// Type coertion
// If I add a number to a string, then the result will be a string
// and the number will be changed into a string as well

variable.addEventListener('change', event => {
    // Increment my number
    console.log(variable.value)
    incrementByNumber = parseInt(event.target.value)

    console.log(incrementByNumber)
})

addButton.addEventListener('click', event => {
    const resultNumber = parseInt(textToChange.innerText)

    const newValue = resultNumber + incrementByNumber

    // Set the value to the div

    textToChange.innerText = newValue

    localStorage.setItem(LOCALSTORAGE_KEY, newValue.toString())

})

removeBtn.addEventListener('click', event => {
    const resultNumber = parseInt(textToChange.innerText)

    let newValue = resultNumber - incrementByNumber

    // Set the value to the div

    if(newValue < 0) {
        newValue = 0
    }

    textToChange.innerText = newValue

    localStorage.setItem(LOCALSTORAGE_KEY, newValue)

})

resetBtn.addEventListener('click', event => {
    // Set the value to the div
    textToChange.innerText = 0

    localStorage.setItem(LOCALSTORAGE_KEY, 0)
})

const localStorageValue = localStorage.getItem(LOCALSTORAGE_KEY)

if(localStorageValue) {
    textToChange.innerText = localStorageValue
}