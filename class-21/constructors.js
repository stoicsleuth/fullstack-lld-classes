// const HPlaptop = {
//     brand: "HP",
//     price: 60000,
//     processor: "AMD",
//     ram: 8
// }

// const DellLaptop = {
//     brand: "Dell",
//     price: 60000,
//     processor: "AMD",
//     ram: 8
// }

// const AppleLaptop = {
//     brand: "Apple",
//     price: 60000,
//     processor: "m1",
//     ram: 8
// }


// function createLaptop(brand, price, processor, ram) {
//     const newLaptop = {}

//     newLaptop.brand = brand
//     newLaptop.price = price
//     newLaptop.processor = processor
//     newLaptop.ram = ram

//     return newLaptop
// }

// const HPlaptop = createLaptop('HP', 40000, 'AMD', 8)
// const AppleLaptop = createLaptop('Apple', 40000, 'AMD', 8)

// console.log(HPlaptop, AppleLaptop)


// This function is called a constructor
function Laptop(brand, price, processor, ram) {
    this.brand = brand
    this.price = price
    this.abcd = brand
    this.processor = processor
    this.ram = ram
}

// Adding a new keyword does three things
// 1: It creates an empty object
// Then "this" inside of the function will now point to this empty object
// 3: This new object is now returned

const HPLaptop = new Laptop('HP', 40000, 'AMD', 8)
const DellLaptop = new Laptop('Dell', 40000, 'AMD', 8)

console.log(HPLaptop)


function Student(name, batch, favorite) {
    this.name = name
    this.batch = batch
    this.favorite = favorite
    this.country = "India"
}

const student1 = new Student('Hiran', 'Morning FE', 'JS')
const student2 = new Student('Kiran', 'Morning FE', 'CSS')

console.log(student1, student2)