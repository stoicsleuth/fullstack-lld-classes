// "use strict";

// Case 1: GLobal context -> empty object
// console.log("The value of this is", this)



// // Case 2: Function when called globally 

// function test () {
//     console.log("Value of this is", this)
// }

// test()


// Case 3: Object Method

// If we are calling a method which is tied to an object
// The value of this will be pointing to that same object itself
const superhero = {
    name: 'Batman',
    planet: {
        name: "Earth",
        callName: function() {
            console.log('Calling from planet', this, this.name)
        }
    },
    shout: function() {
        console.log("Shouting from my object", this)
    },
    run: function() {
        // console.log(this, "Logging from run")
        // console.log("Try to access name", this.name)

        function runFast(){
            // console.log("Try to access name", this.name)
            console.log("Running faster now", this)
        }

        runFast()
    },
}

// superhero.shout()

superhero.run()

// superhero.planet.callName()
// superhero.runFast()