const express = require('express')
const fs = require("fs")

const PORT = 8080

const app = express()

// Use a middleware
app.use(express.json())

const products = JSON.parse(fs.readFileSync("data.json", "utf-8"))

// GET Request
app.get('/products', (req, res) => {
    res.send(products)
})

// POST Request (Adding a product)
app.post('/products/add', (req, res) => {
    const product = req.body
    products.push(product)
    fs.writeFileSync("data.json", JSON.stringify(products), "utf-8")

    res.status(200).json(product)
})


// PUT Request (Updating a request completely)
app.put("/products/update-complete", (req, res) => {
    const productOriginal = req.body

    // Find the product
    let foundProductIndex = products.findIndex((product) => product.id === productOriginal.id)
    
    if(foundProductIndex === -1 ) {
        // Create new
        products.push(productOriginal)
    } else {
        products[foundProductIndex] = productOriginal
    }
    
    fs.writeFileSync("data.json", JSON.stringify(products), "utf-8")
    res.status(200).json(productOriginal)
})



// PATCH Request (Updating a record by any field)
app.patch("/products/update-field", (req, res) => {
    const productOriginal = req.body

    // Find the product
    let foundProductIndex = products.findIndex((product) => product.id === productOriginal.id)
    
    if(foundProductIndex === -1 ) {
        // Send 404 error
        res.status(404)
    } else {
        products[foundProductIndex] = productOriginal
    }
    
    fs.writeFileSync("data.json", JSON.stringify(products), "utf-8")
    res.status(200).json(productOriginal)
})


// DELETE: Deletes a resource
app.delete("/products/delete", (req, res) => {
    const idToDelete = req.body.id

    // Find the product
    let foundProductIndex = products.findIndex((product) => product.id === idToDelete)
    
    if(foundProductIndex === -1 ) {
        // Send 404 error
        res.status(404)
    } else {
        products.splice(foundProductIndex, 1)
    }
    
    fs.writeFileSync("data.json", JSON.stringify(products), "utf-8")
    res.status(200).json(idToDelete)
})


app.listen(PORT, () => {
    console.log('Server has started')
})



