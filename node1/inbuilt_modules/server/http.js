const http = require('http')

const data = {
    name: 'Bruce'
}

const server = http.createServer((req, res) => {
    console.log("Server is triggered", req)

    res.setHeader("Content-Type", "text/html")
    res.setHeader("Some-data", "datavalue")
    res.end("<h1>Header2</h1>")

    // res.end(JSON.stringify(data))
})

//localhost:3000
server.listen(8000)