console.log("FS Module")
const fs = require('fs')

// 1. Reading a file
const content = fs.readFileSync('f1.txt', 'utf-8')
console.log(content)

const contentNew = "This is a completely new content"
const contentExisting = "This is existing content"
// Writing a file?
// Completely new file
// If there is no such file, create the file
fs.writeFileSync("f3.txt", contentNew)

// If there is some data already, update that data
fs.writeFileSync("f2.txt", contentNew)

// Append some data
fs.appendFileSync("f3.txt", "This is my appended text")

// Delete some file using fs
fs.unlinkSync("f3.txt")

// Make a new directory in this folder
// fs.mkdirSync("newdirectory")

// fs.mkdirSync("newDirectory2/internal1/internal2", { recursive: true})

// Existence of a file
const doesExist = fs.existsSync("f1.txt")
console.log(doesExist)