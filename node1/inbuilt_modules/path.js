const path = require("path")

// Utility that joins multiple folders and files using "/"
const path1 = path.join("dir1", "dir2", "dir3", "test.txt")
console.log(path1)

const absPath = "/Users/tara/scaler/classes/fullstack-lld-classes/node1/inbuilt_modules/file.txt"

const basePath = path.basename(absPath)
const dirPath = path.dirname(absPath)
const extName = path.extname(absPath)
console.log({ basePath, dirPath, extName })

const allInfo = path.parse(absPath)
console.log({ allInfo})

// HW for you
// Using path & fs: Copy one file from test.txt to test1.txt