let totalTime = 0;
let resolvePromiseFunc = (delay, value) => {
    console.log('x', delay, value);
    totalTime += delay;

    console.log(totalTime)
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(value)
        }, delay)
    })
}
let a = resolvePromiseFunc(1000, 1);
a.then(async (a) => {
    let b = resolvePromiseFunc(2000, 2);
    let c = resolvePromiseFunc(1000, 3);
    let d = resolvePromiseFunc(2000, 4);
    let e = resolvePromiseFunc(1000, 5);

    // Interesting tidbit -> Because all these await operations
    // Are happening inside a single statement, that's why they are
    // Triggered in parallel
    console.log("totalTime", totalTime, a + await b + await c + await d + await e);
});