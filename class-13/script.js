// We need to ask for information from the server
// We need to call the API with the relevant request

const daysArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]


const temperatureField = document.querySelector(".temp");
const cityField = document.querySelector(".time_location p");
const dateField = document.querySelector(".time_location span");
const emojiField = document.querySelector(".weather_condition img");
const weatherField = document.querySelector(".weather_condition span");
const searchField = document.querySelector(".searchField");
const form = document.querySelector("form");
const errorText = document.querySelector(".errorText")
// Submit event is an event specific to forms

form.addEventListener("submit", event => {
    // Every event in JS has some default behavior attached to it
    // For form submit, it is refrshing the page
    // We need to prevent that

    // This will prevent any default behvaior for an event
    event.preventDefault()
    const search = searchField.value

    // If search is not string
    // Or if there is any number here
    // Reg exp
    // Alert the user by using alert() function

    getWeatherForCity(search)
})

async function getWeatherForCity(city) {
    // 1. Call the API
    try {
        const response = await fetch(`https://api.weatherapi.com/v1/current.json?key=7899daaf05414dfea33153856230910&q=${city}&aqi=yes`)

        const data = await response.json()
    
        console.log(data)

        // So we have the data now
        // Next step is to use this data & update our DOM

        const currentTemp = data.current.temp_c
        const currentCondition = data.current.condition.text
        const conditionImg = data.current.condition.icon

        const locationName = data.location.name
        const localTime = data.location.localtime

        // Split localtime into Time & Date
        const localTimeAsArray = localTime.split(" ")

        const localTimeAsJSDate = new Date(localTimeAsArray[0])

        const dayOfTheWeek = daysArray[localTimeAsJSDate.getDay()]

        temperatureField.innerText = currentTemp
        cityField.innerText = locationName
        dateField.innerText = `${dayOfTheWeek} ${localTimeAsJSDate.getDate()} ${localTimeAsArray[1]}`
        weatherField.innerText = currentCondition
        emojiField.src = conditionImg 
        // emojiField.setAttribute("src", conditionImg)


        // Reset my search field
        searchField.value = ""

        // Reset/hide my error
        errorText.style.display = "none"
    } catch(e) {
        console.log(e)

        const errorMessage = e.message

        console.log(errorText)
        errorText.innerText = "Please enter a valid location"
        errorText.style.display = "block"

        return;
    }

}

getWeatherForCity("Kolkata")