// const promise1 = new Promise((resolve, reject) => {
//     // We need resolve/reject tp inform JS
//     // about the status of our operation. Whether we have failed or succeeded

//     const a = 3
//     const b = 4

//     if(a == b) {
//         resolve("This is successful")
//     } else {
//         reject("This has failed")
//     }
// })

// const promise2 = new Promise((resolve, reject) => {
//     // We need resolve/reject tp inform JS
//     // about the status of our operation. Whether we have failed or succeeded
// })

// const promise3 = new Promise((resolve, reject) => {
//     // We need resolve/reject tp inform JS
//     // about the status of our operation. Whether we have failed or succeeded

//     const a = 3
//     const b = 3

//     if(a == b) {
//         resolve("This is successful")
//     } else {
//         reject("This has failed")
//     }
// })

// console.log(promise2, promise3)


const promise1 = new Promise((resolve, reject) => {
    // We need resolve/reject tp inform JS
    // about the status of our operation. Whether we have failed or succeeded

    setTimeout(() => {
        resolve("Resolved now")
        console.log(promise1)
    }, 2000)
})

console.log(promise1)