const fs = require("fs")


// // Parrallel async operations
const filePromise1 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file1.txt", "utf8")
// // const filePromise2 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file2.txt", "utf8")
// // const filePromise3 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file3.txt", "utf8")




function callbackFn1 (content) {
    console.log("The content of my file is", content)
    const filePromise2 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file2.txt", "utf8")

    // Return the newly created promise
    return filePromise2
}

function callbackFn2 (content) {
    console.log("The content of my file is", content)
    const filePromise3 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file2.txt", "utf8")

    // Return the newly created promise
    return filePromise3
}

function callbackFn3() {
    console.log("All files are read")

    return 5
}


// Be use of chaining, we can turn the result of .then() into another promise
filePromise1.then(callbackFn1).then(callbackFn2).then(callbackFn3).finally(() => {
    console.log("All my promises are resolved/rejected")
})

// .finally() will run if a promise has been coompleted, regardless of the success/failure status

// const newPromise = new Promise((res, rej) => {
//     setTimeout(() => {
//         res(5)
//     }, 2000)

// })

// If I return any value (other than a promise)
// from .then callback, then it actually
// Returns a promise which resolves with the return value

// newPromise.then((val) => {
//     return val + 10

//     setTimeout(() => {
//         res(5)
//     }, 2000)

//     // new Promise((res, rej) => {
//     //     res(val+10)
//     // })
// }).then((val) => {
//     return val + 10
// }).then(val => {
//     console.log(val)
// })