const fs = require("fs")


// Parrallel async operations
const filePromise1 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file1.txt", "utf8")
const filePromise2 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file2.txt", "utf8")
const filePromise3 = fs.promises.readFile("/Users/tara/scaler/classes/fullstack-lld-classes/class-25/file3.txt", "utf8")


function callbackFn (content) {
    console.log("The content of my file is", content)
}

filePromise1.then(callbackFn)

filePromise2.then(callbackFn)

filePromise3.then(callbackFn)

console.log(filePromise1, filePromise2, filePromise3)