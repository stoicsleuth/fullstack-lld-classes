const promise1 = new Promise((resolve, reject) => {
    // We need resolve/reject tp inform JS
    // about the status of our operation. Whether we have failed or succeeded
    

    setTimeout(() => {
        // Mock API response
        const apiResponse = ['Batman', 'Superman', 'Spider-man']

        const apiResponse2=  ['Batman', 'Superman', 'Spider-man']
        // resolve({ apiResponse: apiResponse, apiResponse2, apiResponse2})
        console.log(promise1)

        reject('err')
    }, 2000)
})


promise1.then((error) => {
    console.error(error);

    return new Promise()
});
  
promise1.catch((error) => {
    console.log("j")
    console.error(error);
  });

// promise1.catch((reason) => {
//     console.log("This has failed for some reason", reason)
// })
// // The argument of my first then would be whatever I have resolved it with
// promise1.then((value) => {
//     console.log("This is being called after my promise is resolved")
//     // console.log("And the value of resoluton is", value)
// })

// promise1.then((value) => {
//     console.log("This is also being called after my promise is resolved")
//     console.log("And the value of resoluton is", value)
// })

// promise1.then((value) => {
//     console.log("This is also being called after my promise is resolved part 2")
//     console.log("And the value of resoluton is", value)
// })





// console.log(promise1)