const express = require('express')

const PORT = 8080

const app = express()

app.use(express.json())

// Define the routes
app.get("/", (req, res) => {
    res.send("Hello express")
})

app.get("/about", (req, res) => {
    res.send("This is the About API")
})

app.get("/home", (req, res) => {
    console.log(req.params)
    res.send("This is HOME Api")
})


// This is how we grab a query parameters
app.get("/products", (req, res) => {
    const page = req.query.page
    console.log(page)
    res.send("This is HOME About Api")
})

// This is how we grab express params
app.get("/product/:id/:name", (req, res) => {
    const params = req.params
    console.log(params)
    res.send("This is HOME About Api")
})



// I want to add a post request to add a new user

let users = [{
    id: 1,
    name: "Kumar"
}, 
    {
        id: 2,
        name: "Ruchi"
    }
]




// Add an auth middleware, so that requests before hitting the APIs
// Check that if the user is authenticated

const authenticate = (req, res, next) => {
    const password = req.headers.authorization

    console.log(password)

    if(password === "test") {
        next()
    } else {
        res.status(401).json({ error: "You are not authorized"})
    }
}

const testMiddleWare = (req, res, next) => {
    console.log("I am doing nothing")

    next()
}

// This is how I "register" a middleware
// This will add the middleware for ALL routes
// app.use(authenticate)

app.get("/users", authenticate, (req, res) => {
    res.send(users)
})

// Add a new user
app.post("/users/add", [authenticate, testMiddleWare], (req, res) => {
    const newUser = req.body

    // Check if name is actually present
    if(!req.body.name) {
        res.status(400).json({ error: "Please add user name"})
    }

    const newId = users[users.length - 1].id + 1
    newUser.id = newId

    users.push(newUser)

    res.send("User added successfully")
})

app.delete("/users/delete/:id", (req, res) => {
    const idToDelete = req.params.id

    const userIndex = users.findIndex((user) => user.id === idToDelete)
    users.splice(userIndex, 1)

    // users = users.filter((user) => user.id !== idToDelete)

    res.send("User deleted successfully")

})


app.listen(PORT, () => {
    console.log('Server has started')
})



