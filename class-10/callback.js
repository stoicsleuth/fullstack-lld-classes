function example1() {
  console.log('Called from example1')
}

const num1 = 1
const num2 = 2

function example2(no1, no2, fn) {
  console.log(no1 + no2)
  fn()
  console.log('Called from example2')
}


// console.log(example1())
// console.log(example2(num1, num2, example1))



// HOF -> Value of the function is increased
function sumofSquare(a, b, sqr) {
  const aSquare = sqr(a)
  const bSquare = sqr(b)

  return aSquare + bSquare
}

function sqaureOfAnumber(a) {
  return a * a
}

console.log(sumofSquare(1, 2, sqaureOfAnumber))