const radius = [2, 4, 7, 3, 7]

// Calculate circumference
// Calculate diameter
// Calculate area

function loopAndCalculate(arr, calculate) {
  const answers = []

  for(let i = 0; i <arr.length; i++) {
    answers[i] = calculate(arr[i])
  }

  return answers
}

function calculateDiameter(radius) {
  return radius * 2
}

function calculateCircumference(radius) {
  return 2 * Math.PI * radius
}

function calculateArea(radius){
  return Math.PI * radius * radius
}


console.log('Diameters', loopAndCalculate(radius, calculateDiameter))
console.log('Circumference', loopAndCalculate(radius, calculateCircumference))
console.log('Area', loopAndCalculate(radius, calculateArea))


function example () {
  return 4
}

console.log("Hello", example())