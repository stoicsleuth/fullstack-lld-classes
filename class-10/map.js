// const arr =  [ 1, 2, 3, 4,5]

// // const results = []

// // for(let i = 0; i < arr.length; i++) {
// //   results[i] = arr[i] + 10
// // }

// // console.log(results)

// function transformer(elem, index) {
//   return elem + 10
// }

// const newArray = arr.map(transformer)

// console.log(arr)
// console.log(newArray)

// function transformerFilter(elem) {
//   // Returns true if condition is met, returns false otherwise
//   return elem % 2 == 0
// }


// const newArray2 = arr.filter(transformerFilter)
// console.log(arr)
// console.log(newArray2)


// // ForEach

// function action(elem) {
//   console.log(elem + 10)

//   return elem+10
// }

// arr.forEach(action)

// Every -> boolean (Either true or false)

// const arr = [12, 7, 12, 4]

// function condition(elem) {
//   return elem > 0
// }

// console.log(arr.every(condition))


// Some -> boolean (Either true or false)
// If Even a single element satisfies the condition, it will return True, otherwise false

// const arr = [-12, -7, -12, -4]

// function condition(elem) {
//   return elem > 0
// }

// console.log(arr.some(condition))


const array = [1, 2, 3, 4]

// let sum = 0

// for(let i =0 ; i < array.length; i++) {
//   sum = sum + array[i]
// }

// console.log(sum)

// const result = array.reduce(function(acc, elem, index) {
//   console.log(index)
//   acc = acc + elem

//   return acc
// }, 0)

// console.log(result)


function subtract(b, a) {
  return a - b
}

console.log(subtract(10, 3))
