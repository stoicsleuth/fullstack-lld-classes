transactions = [100, 200, -400, 0, 1000, -700, -500, 2000, 0]

// Task for Day 1 - figure out which ones are credit

// const creditTransactions = []

// for(let i = 0; i < transactions.length; i++){
//   if(transactions[i] > 0) {
//     creditTransactions.push(transactions[i])
//   }
// }

// console.log(creditTransactions)

// // 

// const debitTransactions = []

// for(let i = 0; i < transactions.length; i++){
//   if(transactions[i] < 0) {
//     debitTransactions.push(transactions[i])
//   }
// }

// console.log(debitTransactions)

// Day 3: Minimizing redundant code using HOFs

function checkIsCredit(a) {
  return a > 0
}

function checkIfDebit(a) {
  return a < 0
}

function checkIfZero(a) {
  return a == 0
}


function loopAndCheckSomething(arr, checkFunction) {
  const initialArray = []

  for(let i = 0; i < arr.length; i++){
    if(checkFunction(arr[i])) {
      initialArray.push(arr[i])
    }
  }

  return initialArray
}

console.log('Credit transactions are -', loopAndCheckSomething(transactions, checkIsCredit))
console.log('Debit transactions are -', loopAndCheckSomething(transactions, checkIfDebit))
console.log('Zero rated transactions are -', loopAndCheckSomething(transactions, checkIfZero))


