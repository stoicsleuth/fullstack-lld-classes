function placeOrder(drink) {
    return new Promise((res, rej) => {
        if(drink === "coffee") {
            setTimeout(() => {
                res("Your order of coffee is confirmed")
            }, 2000)
        } else {
            rej("We don't have stock!")
        }
    })
}

function processOrder(orderPlace) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res(`${orderPlace} and served`)
        }, 2000)
    })
}

function generateBill(orderProcess) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res(`${orderProcess} and bill is here!`)
        }, 2000)
    })
}

// When we chain .then() with a previous .then()
// And re return any value other than a promise,
// Then the same value can be accessed as ar argument
// To the next .then()
// placeOrder("coffee").then((val) => {
//     // console.log("Am I here?")
//     console.log(val)
//     return val
// }).then((orderStatus) => {
//     const orderProcessed = processOrder(orderStatus)
//     return orderProcessed
// }).then((orderProcessed) => {
//     console.log(orderProcessed)

//     return orderProcessed
// }).then((orderProcessed) => {
//     const billGenerated = generateBill(orderProcessed)

//     return billGenerated
// }).then((billGenerated) => {
//     console.log(billGenerated)
// })

// function placeOrderOfCoffeeNormal(){
//     placeOrder("coffee").then((orderPlace) => {

//         console.log("I am just a console.log")
//         console.log(orderPlaced)
//     })

// }


// Syntactic sugar -> Just another syntax of doing the same thing

async function placeOrderOfCoffee(){
    // await means wait for the promise to be resolved and return the resolved value
    // Till the promise is resolved, it does not run any line of code
    // Which is below it
    const orderPlaced = await placeOrder("coffee")
    console.log(orderPlaced)
    const orderProcessed = await processOrder(orderPlaced)
    console.log(orderProcessed)
    const billGenerated = await generateBill(orderProcessed)
    console.log(billGenerated)

    console.log("Finally I am done!")
}

placeOrderOfCoffee()