// typeof operator

const a = 2
const b = "23"
const c = [12, 2, 3]
const d = {a: "b"}
const e = () => { console.log("func")}

console.log(typeof a)
console.log(typeof b)
console.log(typeof c)
console.log(typeof d)
console.log(typeof e)


// Array.isArray method
// Do not use typeof method for array, because it was result in object

console.log(Array.isArray(c))
console.log(Array.isArray(d))


// 

const infinity = 1/0
console.log(infinity)


// Adding string to number

// While adding, JS tries to coerce something which is not a string into a string
// String > number -> if this fails then try Number > string
const add = 2 + "3"
console.log(add)

// 
const mul = 2 * "3"
console.log(mul)

const mul1 = 2 * "Scaler"
console.log(mul1)

const sqrt = Math.sqrt(-1)
console.log(sqrt)


console.log(typeof NaN)


// A function to check if something is NaN or not, use isNaN
// Same can be used to check if a value is number -> then we would get false for isNaN
console.log(isNaN(mul1))

console.log(isNaN(null)) // Although this is false, it doesn't mean null is a number
console.log(isNaN(undefined))