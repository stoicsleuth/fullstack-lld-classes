// == vs ===

const a = 2 // Number
const b = "2" // String

// == checks for value
// === checks for both value and type

if(a == b) {
    console.log("They are equal")
} else {
    console.log("Not equal at all ")
}

if(a === b) {
    console.log("They are equal")
} else {
    console.log("Not equal at all ")
}