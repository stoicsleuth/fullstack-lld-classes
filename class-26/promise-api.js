// Handling multiples promises


// Let's assume these thrre are mocks for three file uploads
// and we want to wait till all of them are complete
const promise1 = new Promise((res, rej) => {
    setTimeout(() => {
        res(10)
    }, 1000)
})

const promise2 = new Promise((res, rej) => {
    setTimeout(() => {
        rej(20)
    }, 2000)
})

const promise3 = new Promise((res, rej) => {
    setTimeout(() => {
        res(30)
    }, 3000)
})


// Promise.all takes an array of promises
// Then it waits for all of them to be resolved
// Once all of them ae resolved, it returns a promise
// That contains the value of all the resolutions of these promises
// The order in the result array is the same as the order in which we passed the promises

// If there's a single rejection of any of the promises in the array
// It immediately rejects with the rejection value

// Promise.all([promise3, promise2, promise1]).then((val) => {
//     console.log(val)
// }).catch((err) => {
//     console.log(err)
// })

// async function runPromiseAll() {

//     try {
//         const returnValue = await Promise.all([promise3, promise2, promise1])

//         console.log(returnValue)
//     } catch(e) {
//         console.log(e)
//     }

// }

// runPromiseAll()


// This is immediately resolved when the first promise gets resolved
Promise.race([promise1, promise2, promise3]).then((val) => {
    console.log(val)
}).catch((e) => {
    console.log(e)
})