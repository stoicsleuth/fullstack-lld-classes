// Truthy-Falsy

const a = -100


// If any value passes the if condition
// Then the value is called truthy, otherwise falsy
if(a) {
    console.log("Truthy")
} else {
    console.log("Falsy")
}

// List of falsy values : false, undefined, null, 0, '', NaN

const array = []

if(array) {
    console.log("Truthy")
} else {
    console.log("Falsy")
}