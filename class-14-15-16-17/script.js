// STep 1: Select the two elements

const addButton = document.querySelector(".add-btn")
const removeButton = document.querySelector('.remove-btn')
const modalContainer = document.querySelector(".modal-cont")
const allPriorityContainers = document.querySelectorAll(".priority-color")
const textAreaContainer = document.querySelector('.textArea-cont')
const mainContainer = document.querySelector('.main-cont')

const allFilterContainers = document.querySelectorAll('.color')

// document.

let isModalTaskOpen = false
let isDeleteModeActive = false
let isEditingCardContent = false
let currentPriorityColor = "lightpink"


const lockedClass = "fa-lock"
const unlockedClass = "fa-lock-open"

const ticketsArray = []


const colorsArray = ['lightpink', 'lightgreen', 'lightblue', 'black']

const LOCALSTORAGE_KEY = 'tickets'


// Retrieve the value of tickets from localStorage
// And create tasks/tickets if any of them exist

// Step 1: Get the tickets from localStorage

const ticketsInStorage = localStorage.getItem(LOCALSTORAGE_KEY)

if(ticketsInStorage) {
    const parsedTickets = JSON.parse(ticketsInStorage)
    // Create new tickets 
    parsedTickets.forEach((ticket) => {
        createTask(ticket.type, ticket.value, ticket.id)
    })
}


// ---------------------------------------
// These are all our event listerers needed to react to user actions

addButton.addEventListener('click', event => {
    if(!isModalTaskOpen) {
        modalContainer.style.display = "flex"
        textAreaContainer.value = null
        isModalTaskOpen = true
        // Open it
    } else {
        // Close it
        modalContainer.style.display = "none"
        isModalTaskOpen = false
    }
})

removeButton.addEventListener('click', event => {
    // 1. Alert the user that delete mode has been activated
    // And change the value of isDeleteModeActive
    isDeleteModeActive = !isDeleteModeActive


    // 2: Activate the delete mode 

    if(isDeleteModeActive) {
        alert("Delete mode has been activated");
        // 3: Change the color of my delete button
        removeButton.style.color = "red"
    } else {
        alert("Delete mode has been deactivated");
        removeButton.style.color = "white"
    }
})

// Add event listeners to add of the priority containers
// So that when we click, it gets selected

allPriorityContainers.forEach((container) => {
    container.addEventListener('click', () => {
        allPriorityContainers.forEach((container) => {
            container.classList.remove('active')
        })
        container.classList.add('active');

        currentPriorityColor = container.classList[0]

        // createTask()
    })
})


modalContainer.addEventListener('keydown', event => {
    if(event.key == 'Shift') {
        //Create the event
        // STep 1: Get the value of current priority
        // Which is already stored in currentPriorityColor

        // Step 2: Get the value of my textArea
        const textAreaValue = textAreaContainer.value

        // If there is no value of textArea or there is no color selected,
        // Then alert the user apropriately


        // console.log(textAreaValue, !textAreaValue.trim())

        if(!textAreaValue.trim() || !currentPriorityColor) {
            alert('Please make sure you have added a task value and have selected a priority color')
            return
        }

        const shortId = shortid.generate()

        // Create my task
        createTask(currentPriorityColor, textAreaValue, shortId)

        // Reset the textarea value
        textAreaContainer.value = ""

        // Close the modal
        modalContainer.style.display = "none"

        // Reset the variable which was tracking if modal was open or close
        isModalTaskOpen = false

        // Reset currentPriorityColor
        currentPriorityColor = null

    }
})

// Filtering logic

// 1. Select and loop through all my filter color divs and add event listener
allFilterContainers.forEach((container) => {

    container.addEventListener('click', event => {
        // fetching the color
        const filterColor = container.classList[0]
        
        // Filter my list of tasks to get only those that match

        const filteredTasks = ticketsArray.filter((elem) => {
            return filterColor == elem.type
        })
        // Re-generate my DOM using these filtered tasks/tickets

        // Step 1: Get all tickets & remove them from the DOM
        const allTicketDivs = document.querySelectorAll('.ticket-cont')

        allTicketDivs.forEach((ticket) => {
            ticket.remove()
        })

        // Re-create new filtered items to the DOM
        filteredTasks.forEach((filteredTask) => {
            createTask(filteredTask.type, filteredTask.value, filteredTask.id)
        })
        console.log(ticketsArray)
    })


    // To reset, I just need to double click on my colored divs,
    // Which means adding another event listener to this div
    container.addEventListener('dblclick', event => {
        // Remove any tasks, if present in the DOM
        // Step 1: Get all tickets & remove them from the DOM
        const allTicketDivs = document.querySelectorAll('.ticket-cont')

        allTicketDivs.forEach((ticket) => {
            ticket.remove()
        })

        // Re-create all items to the DOM
        ticketsArray.forEach((filteredTask) => {
            createTask(filteredTask.type, filteredTask.value, filteredTask.id)
        })
    })
})



// ---------------------------------------
// These are all functions to handle business logic/adding to dom/updating state & localStorage

// Re-usable function to create a new task that can be used in multiple places
// This function essentially does two things
// 1. Add a div of a ticket to the DOM and add the event listeneres
// 2. Adds the ticket to the application & localStorage, if it does not exist
function createTask(taskType, textValue, taskId) {
    // Actually create the HTML element of my task
    // & append it to the main container

    //STep1 : create the leemnt
    const newDiv = document.createElement('div')
    newDiv.classList.add("ticket-cont")


    // Step 2: Modify the children of my newly created div using innerHTML
    newDiv.innerHTML = `
    <div class="ticket-color ${taskType}"></div>
    <div class="ticket-id">${taskId}</div>
    <div class="task-area">${textValue}</div>
    <div class="ticket-lock">
        <i class="fa-solid fa-lock"></i>
    </div>
    `

    // Check if this task ID already exists, if yes, then don't add it
    const foundTicketIndex = ticketsArray.findIndex((elem) => {
        return elem.id == taskId
    })

    // How do we know if that index wasn't found
    // If findIndex returns us -1
    if(foundTicketIndex == -1){
        const ticketObject = {
            'id': taskId,
            'type': taskType, //Color
            'value': textValue
        }
        ticketsArray.push(ticketObject)

        // I have to save this updated ticketsArray to my localStorage
        localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(ticketsArray))
    }
    // Create a ticket object


    console.log(ticketsArray)


    handleRemoval(newDiv, taskId)

    handleLock(newDiv, taskId)

    handlePriorityChange(newDiv, taskId)

    // STep 3: Append to main
    mainContainer.appendChild(newDiv)
}

function handlePriorityChange(divToBeChanged, id) {
    const coloredDiv = divToBeChanged.querySelector('.ticket-color')

    // Find out index of my ticket 
    const idx = findTicketIdx(id)

    coloredDiv.addEventListener('click', event => {
        // Fetch the color from the div

        const currentColor = coloredDiv.classList[1]

        const index = colorsArray.findIndex((elem) => elem == currentColor)

        // Incremement the index and save it to the class
        const newIndex = (index + 1) % colorsArray.length
        const newColor = colorsArray[newIndex]

        // Remove last color and update with new color
        coloredDiv.classList.remove(currentColor)
        coloredDiv.classList.add(newColor)

        // Here also, I am not updating the data
        // That's why it is forgetting the color whenever filter is applied

        // Update the application state
        ticketsArray[idx].type = newColor

        // Update the localStorage
        localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(ticketsArray)) 
    })
}

// Find out the index of a ticket, given the ticket ID
function findTicketIdx(id) {
    return ticketsArray.findIndex((ticket) => {
        return ticket.id == id
    })
}

// Handling the locking & unlocking mechanism
// This functions enableds the content area of a ticket to be editable
// Based on if the lock is locked/unlocked
function handleLock(divToBeLocked, ticketId) {
    // Add an event listener to my lock
    // Inspect the current class of the lock 
    // Based on that, flip the class 
    // Based on the class, also make the content of the div editable

    const iconElement = divToBeLocked.querySelector(".ticket-lock > i")

    const taskAreaElement = divToBeLocked.querySelector('.task-area')

    iconElement.addEventListener('click', event => {

        // Find out the index of my ticket 
        const idx = findTicketIdx(ticketId)

        // Figure out the class of my icon & make it flipped
        

        const classOfIcon = iconElement.classList[1]

        if(classOfIcon == lockedClass) {
            // Remove the class
            iconElement.classList.remove(lockedClass)
            // Add the unlock class
            iconElement.classList.add(unlockedClass)
            // Make the content editable
            // taskAreaElement.focus()
            taskAreaElement.setAttribute('contenteditable', 'true')

            console.log(ticketsArray)
        } else {
            // Remove the class
            iconElement.classList.remove(unlockedClass)
            // Add the unlock class
            iconElement.classList.add(lockedClass)
            // Make the content uneditable
            taskAreaElement.setAttribute('contenteditable', 'false')

            // Update my application state
            ticketsArray[idx].value = taskAreaElement.innerText
            console.log({ ticketsArray })

            // Update my localStorage
            localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(ticketsArray))
        }
        
    })


}

function handleRemoval(divToBeRemoved, id) {
    //Find the index of the ticket that needs to be removed
    const idx = findTicketIdx(id)

    divToBeRemoved.addEventListener('click', event => {
        // Check if we are in delete mode, then delete it
        // Otherwise do nothing

        if(isDeleteModeActive) {
            // Remove the item
            // Two ways of removing/hiding an item
            // 1: setting display: none
            // 2: Calling remove function
            divToBeRemoved.remove()

            // Fetch the corresponding div id from the lis of divs
            // Remove the item from my array
            ticketsArray.splice(idx, 1)

            
            // Update the localStorage
            localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(ticketsArray)) 
        }
    })

}

