class Laptop {
    // Every class must have an constructor function
    // using which we can instantiate an object of the class
    constructor(brand, price, processor, ram) {
        this.brand = brand
        this.price = price
        this.processor = processor
        this.ram = ram

        // This is how we can add a method in the constructor function
        // Notice how we are using an anonymous function here
        // Because we won't be using the name of the function, but rather
        // The key of the object to which the function is assigned
        // To invoke the function
        this.invoice = function () {
            console.log(`The ${this.brand} laptop costs ${this.price}`)
        }
    }
}

const HPLaptop = new Laptop('HP', 40000, 'AMD', 8)
const DellLaptop = new Laptop('Dell', 40000, 'AMD', 8)

// HPLaptop.abc()
// DellLaptop.invoice()

// console.log(HPLaptop)
// console.log(DellLaptop)


class Person{
    constructor(name, age, city) {
        this.name = name
        this.age = age
        this.city = city

        this.greet = function () {
            console.log('Hey! My name is ', this.name)
        }
    }
}

const person1 = new Person('Kumar', 25, 'Hyderabad')

// This means Student class in inheriting from Person class
class Student extends Person{
    constructor(name, age, city, batch, cgpa, id) {
        // By calling super
        // We are invoking the parent constructor
        // Which is Person for our case
        // Be aware that the order in which we pass the arguments
        // need to match with the parent constructor
        super(name, age, city)
        this.batch = batch
        this.cgpa = cgpa
        this.id = id
    }


    // Static functions do not depend
    // On any arguments or the object
    // That is they exisst on the class, not the object
    static displayCollege() {
        console.log("The college is VIT")
    }
}

const student1 = new Student("Nikhil", 25, 'Bangalore', 'FE', 9, 1)
student1.greet()

Student.displayCollege()
// console.log(student1)

class Teacher extends Person{
    constructor(name, age, city, sub, salary) {
        //If we are extending from a class
        // The first thing we need to do is call super here
        // Before we assign other properties
        super(name, age, city)

        this.sub = sub
        this.salary = salary
    }
}

const teacher1 = new Teacher("Vivek", 25, 'Bangalore', 'JS', 1000000)
teacher1.greet()
// console.log(teacher1)



class Animal{
    constructor(name){
        this.name = name;

        this.speak = function(){
            // this.new = "100"
            // this.newFunction = function () {
            //     console.log(this, 'This is a new function')
            // }
            console.log(this)
            console.log(this.sound)
        }
    }
}

class Dog extends Animal {
    constructor(name){
        super(name);
        this.sound = "Woof!";
    }
}

let dog = new Dog("Buddy");

// dog.something = "100"
dog.speak()
// dog.newFunction()