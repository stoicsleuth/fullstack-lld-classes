//Before ES6 and classes, we could use inheritance
// Like this
function Shape() {
    this.name = "Shape";
}

// This will add it to the constructor function's proto
Shape.prototype.getName = function() {
    return this.name;
}

function Square(side) {
    // This is analogous to calling super in our class
    Shape.call(this);
    this.side = side;
}

// All these following lines are doing
// is basically the same as writing Square extends Shape
Square.prototype = Object.create(Shape.prototype)
Square.prototype.constructor = Square;

Square.prototype.getArea = function() {
    return this.side * this.side;
}
var square = new Square(5)
console.log(square.getName())