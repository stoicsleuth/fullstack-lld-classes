let arr = [100, 6, 200, 1, 21]
arr = arr.sort((a, b) => a - b)
console.log(arr)
// console.log(arr.mysort())

const object1 = {
    property: 10,
    // showProperty: function() {
    //     console.log(this.property)
    // }
    // This is how you can override a function defined on prototype
    toString: function() {
        console.log("I am a sneaky toString")
    }
}
// console.log(object1)


class Person{
    constructor(name, age, city) {
        this.name = name
        this.age = age
        this.city = city
    }

    greet() {
        console.log('Hey! My name is ', this.name)
    }
}

const p1 = new Person("Nikhil", 25, 'Bangalore')
console.log(p1)
p1.greet()

// This means Student class in inheriting from Person class
// This will add Person as a prototype to Student
class Student extends Person{
    constructor(name, age, city, batch, cgpa, id) {
        // By calling super
        // We are invoking the parent constructor
        // Which is Person for our case
        // Be aware that the order in which we pass the arguments
        // need to match with the parent constructor
        super(name, age, city)
        this.batch = batch
        this.cgpa = cgpa
        this.id = id
    }


    // Static functions do not depend
    // On any arguments or the object
    // That is they exisst on the class, not the object
    static displayCollege() {
        console.log("The college is VIT")
    }
}

const student1 = new Student("Nikhil", 25, 'Bangalore', 'FE', 9, 1)
// console.log(student1)
// student1.greet()
// console.log(student1)

class Teacher extends Person{
    constructor(name, age, city, sub, salary) {
        //If we are extending from a class
        // The first thing we need to do is call super here
        // Before we assign other properties
        super(name, age, city)

        this.sub = sub
        this.salary = salary
    }
}

const teacher1 = new Teacher("Vivek", 25, 'Bangalore', 'JS', 1000000)
// console.log(teacher1)


// getPrototypeOf
// This will return the prototype of an object

const studentProto = Object.getPrototypeOf(student1)
const personProto = Object.getPrototypeOf(studentProto)
const classProto = Object.getPrototypeOf(personProto)

const objectProto = Object.getPrototypeOf(classProto)

console.log(objectProto)



// HW question
// Now that we know how to leverage prototype
// Can we add a custom function on every array?
// const arr = [1, 2, 3, 4, 5]
// arr.callme() -> Called from the array

Object.prototype.callMe = function() {
    console.log('I am called from the prototype chain')
}

const arr2 = [1, 2, 3, 4, 5]

// arr2.functionThatShouldNOtexist = function(){
//     console.log('How can I even be called?')
// }
// arr2.functionThatShouldNOtexist()

arr2.callMe()

const arr3 = [1, 2, 3]
arr3.callMe()
