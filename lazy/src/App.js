import './App.css';
import {BrowserRouter , Routes , Route} from 'react-router-dom'
import Navbar from './components/Navbar';
// import Home from './pages/Home'
// import About from './pages/About'
// import Products from './pages/Products'
// import Testimonials from './pages/Testimonials'
// import { moviesData } from './data'
import { useState } from 'react';
import { lazy } from 'react';
import { Suspense } from 'react';
import Feature2 from './components/Feature2';
import BoringButton, { ExcitingButton } from './components/BoringButton';
import Controlled from './components/Controlled';
import UnControlled from './components/Uc';

const Home = lazy(() => import('./pages/Home'))
const Products = lazy(() => import('./pages/Products'))
const Testimonials = lazy(() => import('./pages/Testimonials'))
const About = lazy(() => import('./pages/About'))


function Loader() {
  return (
    <div>
      Loading....
    </div>
  )
}

function App() {
  const [ movies, setMovies] = useState([])

  const handleShowMovies = () => {

    //Dynamic import
    // To be used when loading some JS files, not components
    import('./data').then((module) => {
      console.log('Showing movies')
      setMovies(module.moviesData)
    })   

  }


  return (
    <div className="App">
      <h1>These are the Movies</h1>
      <button onClick={handleShowMovies}>Show Movies</button>

      {movies.length > 0 ? (
        <div>
          {movies.map((movie) => (
            <h2 key={movie.id}>{movie.name}</h2>
          ))}
        </div>
      ): 'There are no movies'}

      <Suspense fallback={<Loader />}>
       <BrowserRouter>

        {/* <Navbar/> */}
    
       <Routes>
{/* 
        <Route path='/' element={<Home/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/products' element={<Products/>}/>
        <Route path='/testimonials' element={<Testimonials/>}/> */}
       </Routes>
     
       
       </BrowserRouter>
       </Suspense>


       {/* <Feature2 />

       <BoringButton />
       <ExcitingButton /> */}


       <Controlled />
       <UnControlled />
      
    </div>
  );
}

export default App;
