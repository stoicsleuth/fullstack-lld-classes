import loadBooksHOC from "./LoadBooksHOC";
import Feature1 from "./Feature1";


const BoringFeature2 = () => {
    const loadMovies = () => {
        console.log("Loaded movies")
      }

    const loadBooks = () => {
        console.log("Loaded books")
    }
    
      return (
        <div>This is my feature 1
            <button onClick={loadMovies}>Load Movies</button>
            <button onClick={loadBooks}>Load Books</button>
        </div>
      )
}

// This component should have the functionality 
// of Feature1 component plus the additional 
// feature of loading books
const Feature2 = loadBooksHOC(Feature1)

export default Feature2