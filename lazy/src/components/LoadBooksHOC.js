

function loadBooksHOC(Component) {
    return function Feature2() {
        // This is the new feature of loading books
        const loadBooks = () => {
            console.log('Loaded books')
        }

        return (
            <div>
                {/* This will have the old feature of loading movies */}
                <Component />
                <button onClick={loadBooks}>Load books</button>
            </div>
        )
    }
}

export default loadBooksHOC