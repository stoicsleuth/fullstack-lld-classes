import React from 'react'
import { useRef } from 'react'
import { useState } from 'react'

function UnControlled() {
  const inputRef = useRef(null)

  return (
    <div>UnControlled
        <input ref={inputRef} type='text' />

        <button onClick={() => {
          console.log(inputRef.current.value)
        }}>Get Value of Input</button>
    </div>
  )
}

export default UnControlled