

function BoringButton({ style, label = "Boring Button" }) {
    return (
        <button style={{padding: "20px 40px", backgroundColor:"greenyellow", ...style  }}>
            {label}
        </button>
    )
}


function boringButtonWithBorderAndColor(Component) {
    return function ExcitingButton() {
        return (
            <Component style={{ backgroundColor: 'black', color: 'white', borderRaidus: "10px", border: '1px solid white'}} label="Exciting Button" />
        )
    }
}

const ExcitingButton = boringButtonWithBorderAndColor(BoringButton)

export default BoringButton

export {
    ExcitingButton
}