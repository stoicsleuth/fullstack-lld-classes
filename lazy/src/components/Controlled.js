import React from 'react'
import { useState } from 'react'

function Controlled() {
  const [value, setValue] = useState("")

  return (
    <div>Controlled
        <button onClick={() => { setValue("void")}}>Change value of input to "Void"</button>
        <input value={value} onChange={(e) => { setValue(e.target.value)}} type='text' />
    </div>
  )
}

export default Controlled