import React from 'react'


// This component is responsible for
// Displaying movies
function Feature1() {
  const loadMovies = () => {
    console.log("Loaded movies")
  }

  return (
    <div>This is my feature 1
        <button onClick={loadMovies}>Load Movies</button>
    </div>
  )
}

export default Feature1