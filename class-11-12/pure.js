const arr = [1, 2, 3, 4]

//Impure function
function addElementToArray(arr1) {
    arr1.push(5)

    return arr1
}

console.log(addElementToArray(arr)) // [1, 2, 3, 4, 5]
console.log(addElementToArray(arr)) // [ 1, 2, 3, 4, 5, 5]
console.log(addElementToArray(arr)) //[ 1, 2, 3, 4, 5, 5, 5]

//Pure function
function addElementToArray(arr1) {
    const arr2 = Array.from(arr1)
    arr2.push(5)

    return arr2
}

console.log(addElementToArray(arr)) // [1, 2, 3, 4, 5]
console.log(addElementToArray(arr)) // [ 1, 2, 3, 4, 5]
console.log(addElementToArray(arr)) //[ 1, 2, 3, 4, 5]