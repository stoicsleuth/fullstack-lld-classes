// const arr = [3, 5, 4, 1, 2]


// if(!Array.prototype.customSort) {
//     Array.prototype.customSort = function() {
//         console.log("I am a custom function")
//     }
// }

// const arr2 = arr.customSort()


// console.log(arr2)

// The method of adding custom implementation of a an inbuilt funtion
// is called a polyfill, because we can't gurantee that one in built fn 
// will be present in all JS engines all the time


const arr = [1, 2, 3, 4]

function callback(a) {
    return a + 10
}


// This check is very important
// because we don't want to overwrite any in build fn implementation
if(!Array.prototype.myMap) {
    Array.prototype.myMap = function(callback) {
        // Loop over the array, run the callback for each of these entries
        // and store the value in a new array

        // this inside this function will be pointing to the array on which the function was called

        const newArray = []

        for(let i = 0; i < this.length; i++) {
            const newEntry = callback(this[i])
            newArray.push(newEntry)
        }

        return newArray
    }
}

const arr2 = arr.myMap(callback)
// console.log(arr2)


function callback2(a) {
    return a % 2 === 0
}

if(!Array.prototype.myFilter) {
    Array.prototype.myFilter = function(callback) {
        const newArray = []

        for(let i = 0; i < this.length; i++) {
            if(callback(this[i])) {
                newArray.push(this[i])
            }
        }

        return newArray
    }
}

const arr3 = arr.myFilter(callback2)
console.log(arr3)




if(!Array.prototype.myReduce) {
    Array.prototype.myReduce = function(callback, initialValue) {
        if(this.length <= 0) {
            return initialValue
        }

        let finalValue = undefined
        let startIndex = undefined

        if(!initialValue) {
            finalValue = this[0]
            startIndex = 1
        } else {
            finalValue = initialValue
            startIndex = 0
        }

        for(let i = startIndex; i < this.length; i++) {
            // Run my callback function with acc and current index
            // Get the new value and assign it to acc
            finalValue = callback(finalValue, this[i])
        }

        return finalValue
    }
}

const arr4 = [].myReduce(function(acc, currentArrayValue) {
    return acc + currentArrayValue
}, 100)
console.log(arr4)

// Implement polyfills for Array.some and Array.every