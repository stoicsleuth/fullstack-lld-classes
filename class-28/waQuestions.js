
// const promise = new Promise(res => res(2));
// promise.then(v => { // 2
//         console.log(v);
//         return v * 2; //4
//     })
//     .then(v => {  // 4
//         console.log(v);
//         return v * 2; //8
//     })
//     .finally(v => { // undefined
//         console.log(v);
//         return v * 2;
//     })



const tom = () => console.log('Tom');

const jerry = () => console.log('Jerry');

const cartoon = () => {
  console.log('Cartoon'); // Cartoon is logged to the console

  setTimeout(tom, 5000); // this goes to callback queue

  new Promise((resolve, reject) =>
    resolve('should it be right after Tom, before Jerry?')
  ).then(resolve => console.log(resolve)) // microtask queue

  jerry(); // Jerry is logged to the console
}

// cartoon();


function add(x){
    return function(y){
        return function namedFn(z){
            return x+y+z;
        }
    }
}

var result = add(2)(3,4, 4, 5, 5, 56);

console.log(result)