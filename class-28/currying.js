// Q1Add two numbers
// sum(2, 3)
// div(2, 3)
// mul(2, 3)
// calc(2, 3, 'add')
// calc(a, b, c)
// function sum(a, b) {
//     return a + b
// }

// Sum of 2 & 3
// sum(2)(3)

// Currying: Using JS closures to build arguments and finally using them in an inner function is called currying.

function sum(a) {
    return function inner (b) {
        return a + b
    }
}
// const innerFn = sum(2)
// const result = innerFn(3)

const result = (sum(2))(3)

// console.log(result)


// Q2. calc('sum')(2)(3) -> sum/mul/div/sub
// 2 *3, div -> 2/3, sub -> 2-3

function calc(operation){
    return function inner1 (a) {
        return function inner2(b) {
            if(operation === 'add') {
                return a + b
            }
            if(operation === 'sub') {
                return a - b
            }
            if(operation === 'mul') {
                return a * b
            }
            if(operation === 'div') {
                return a / b
            }
        }
    }
}

// This technique of partially applying a feature and getting a new function
// is called partial application
const multiplyFn = calc("mul")

const divFn = calc("div")

const result1 = divFn(2)(3)
// console.log(result1)


// Q3: sum(1)(), sum(3)(4)(4)() , sum(4)(7)()

function sum(a) {
    return function inner(b) {
        if (b) {
            return sum(a+b)
        } else {
            return a
        }
    }
}

// const sum = a => b => b ? sum(a+b) : a

console.log(sum(1)(2)())
console.log(sum(2)(4)(9)())




// Assignment Q
//

function multiplyBy(num) {
    return function (x) {
        return num * x
    }
}

// Parial application
const mulByTwo = multiplyBy(2)
const resultMul = mulByTwo(5)

console.log(resultMul)