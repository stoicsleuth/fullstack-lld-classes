import { configureStore } from "@reduxjs/toolkit"
import myCartReducer from './cartSlice'
import productsReducer from './productSlice'

// This is how we create a central store in my app
// It is encouraged to have ONLY one central store.
const store = configureStore({
    reducer: {
        cart: myCartReducer,
        products: productsReducer
        // user: userReducer,
        // products : productReducer
    }
})


/// 
// const myContextValue = {
//     cart: ...
//     products: ...configureStore.
// }

export default store