import { createSlice } from "@reduxjs/toolkit"


//add(product1)

const cartSlice = createSlice({
    // This will be the name of my slice
    name: 'cart',
    initialState: [],
    reducers: {
        addToCart(state, action) {
            state.push(action.payload)
        },
        removeFromCart(state, action) {
            return state.filter((item) => item.id !== action.payload.id)
        }
    }
})

// const allActions = cartSlice.actions
// const addAction = cartSlice.actions.add

// export const { add: addAction }

export const { addToCart, removeFromCart } = cartSlice.actions

// This is our main component for this slice, which I have to import
// in my central/main store
export default cartSlice.reducer