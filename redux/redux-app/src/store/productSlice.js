import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from 'axios'

export const STATUSES = {
    LOADING: 'loading',
    SUCCESS: 'success',
    ERROR: 'error'
}

const BASEURL = 'https://fakestoreapi.com'

// This is the RTK way of creating a thunk,
// just by using the createAsyncThunk
export const fetchProducts = createAsyncThunk('products', async (apiURl) => {
    const res = await axios.get(`https://${BASEURL}/${apiURl}`)
    return res.data
} )

const productsSlice = createSlice({
    // This will be the name of my slice
    name: 'products',
    initialState: {
        data: [],
        status: STATUSES.SUCCESS
    },
    reducers: {
        setProducts(state, action) {
            state.data = action.payload
        },
        // setStatus(state, action) {
        //     state.status = action.payload
        // }
    },
    extraReducers: (builder) => {
        builder.addCase(fetchProducts.pending, (state) => {
            state.status = STATUSES.LOADING
        }).addCase(fetchProducts.fulfilled, (state, action) => {
            state.data = action.payload
            state.status = STATUSES.SUCCESS
        }).addCase(fetchProducts.rejected, (state) => {
            state.status = STATUSES.ERROR
        })
    }
})

// Redux Thunk -> Just a function which returns another function
// That performs some asynchronous work, which also has access
// to "dispatch"

export const { setProducts, setStatus } = productsSlice.actions


// This is the old way of creating a THUNK
// export function fetchProducts(apiURl) {
//     return async function fetchProductThunk(dispatch) {
//         dispatch(setStatus(STATUSES.LOADING))

//         try {
//             const res = await axios.get(`https://${BASEURL}/${apiURl}`)
//             dispatch(setProducts(res.data))
    
//             dispatch(setStatus(STATUSES.SUCCESS))
//         } catch (error) {
//             dispatch(setStatus(STATUSES.ERROR))
//         }
//     }
// }



export default productsSlice.reducer