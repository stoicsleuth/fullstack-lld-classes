import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

function Navbar() {

  //Fetch actual no of items from store
  const items = useSelector((state) => state.cart)

  console.log({ items })


  return (
    <div style={{
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'space-between'
    }}>
        <span className='logo'>Redux store</span>
        <div> 
            <Link className="navLink" to="/">Home</Link> 
            <Link className="navLink" to="/cart">Cart</Link> 
            <span className='cartCount'>Cart items: {items.length} </span>
        </div>
    </div>
  )
}

export default Navbar