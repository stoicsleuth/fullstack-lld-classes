import { useDispatch, useSelector } from "react-redux"
import { removeFromCart } from "../store/cartSlice"

function Cart() {
  const items = useSelector((state) => state.cart)
  const dispatch = useDispatch()

  const removeFromCartFn = (product) => {
    dispatch(removeFromCart(product))
  } 

  return (
    <div>
        <div className="cartWrapper">
            {items.map((item) => (
                <div className="cartCard">
                    <img src={item.image} />
                    <h5>{item.title}</h5>
                    <h5>Price: $ {item.price}</h5>

                    <button className="remove-btn" onClick={() => removeFromCartFn(item)}>Remove from cart</button>
                </div>
            ))}
        </div>
    </div>
  )
}

export default Cart