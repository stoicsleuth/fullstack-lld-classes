import { useEffect } from 'react'
import { useState } from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { addToCart, removeFromCart } from '../store/cartSlice'
import { STATUSES, fetchProducts } from '../store/productSlice'

// Custom hooks are just functions
// That "use" some of react's inbuild hooks
// The reason we want to do that is DRY
// Because if you have to trigger the same API request from multiple
// places, instead of having useEFfect in all those components
// You can define a custom hook that does it for you
function useAPI() {
    const productsSlice = useSelector((state) => state.products)
    const products = productsSlice.data
    const status = productsSlice.status
  
    const dispatch = useDispatch()
  
    useEffect(() => {
      const getProducts = async () => {
      dispatch(fetchProducts('products'))
      }
  
      getProducts()

      return () => {
        console.log("Unmounting")
      }
    }, [])


    return {
        products,
        status
    }
}


function Product() {
  const {products, status} = useAPI()

  const cartItems = useSelector((store) => store.cart)
  const dispatch = useDispatch()

  const cartItemsId = cartItems.map((cartItem) => cartItem.id)
  console.log({ cartItemsId })

 
  const handleAddToCart = (product) => {
    // Actually "dispatch" the action of adding to cart
    dispatch(addToCart(product))
  }

  const handleRemoveFromCart = (product) => {
    dispatch(removeFromCart(product))
  }

  if(status === STATUSES.ERROR) {
    return (
        <div>Error!!</div>
    )
  }


  return (
    <div>
        <div className='productsWrapper'>
            {status === STATUSES.LOADING ? (
                <div>Loading</div>
            ) : products.map((product) => (
                <div key={product.id} className='card'>
                    <img src={product.image} />
                    <h6>{product.title}</h6>
                    <h5>$ {product.price}</h5>
                    {cartItemsId.includes(product.id) ? (
                     <button onClick={() => handleRemoveFromCart(product)} className='btn'>Remove from cart</button>
                    ): (
                        <button onClick={() => handleAddToCart(product)} className='btn'>Add to cart</button>
                    )}
                </div>
            ))}
        </div>
    </div>
  )
}

export default Product



// const func = () => { return 5 }
// const func = () => 5