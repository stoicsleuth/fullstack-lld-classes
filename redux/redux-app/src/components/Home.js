import Product from "./Product"

function Home() {

  return (
    <div>
        <h2 style={{ textAlign: 'center '}}>Snapdeal</h2>
        <Product />
    </div>
  )
}

export default Home