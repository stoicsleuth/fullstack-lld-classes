import { useState } from "react";
import uuid from 'react-uuid';
import {TiTick, TiTrash} from 'react-icons/ti';
import { useReducer } from "react";
import { formReducers, taskReducers } from "./reducers";



const TodoReduced = () => {
  const [list, dispatch ]= useReducer(taskReducers, [])

  const [task, dispatchTask ]= useReducer(formReducers, {
    title: "",
    by: ""
  })

  
  const handleTask = (e) => {
    e.preventDefault();

    dispatchTask({
        type: "HANDLE_TASK",
        key: e.target.name,
        payload: e.target.value
    })
  }

  const addTask = () => {
    dispatch({
        type: "ADD_TASK",
        payload: task
    })
  }

  const markDone = (id) => {
    dispatch({
        type: "DONE_TASK",
        payload: id
    })
  }

  const deleteTask = (id) => {
    dispatch({
        type: "REMOVE_TASK",
        payload: id
    })
  }


//   <Context value={list, dispatch} ></Context>

  return (
    <>
      <div>
        <h1>My TodoList</h1>
        <div>
          I want to do <input type="text" name="title" onChange={handleTask}/> by{" "}
          <input type="date" name="by" onChange={handleTask} />
          <button className="wishBtn" onClick={addTask}>Add a Task</button>
        </div>
        <ul>
          {list.map((item) => (
            <li key={item.id}>
              <span style={{ textDecoration: item.isDone ? "line-through" : "" }}>
                <strong>{item.title}</strong> is due by {item.by}</span>
              <span><TiTick size={24} onClick={() => markDone(item.id)} /></span>
              <span><TiTrash size={24} onClick={() => deleteTask(item.id)}/></span>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default TodoReduced;