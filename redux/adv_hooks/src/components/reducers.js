
// if(action.type === "ADD") {
//     do this
// } else if(action.type === "REMOVE") {
//     do that
// }else if(action.type === "DUPLICATE") {
//     dp this
// }

// switch(action.type) {
//     case 'ADD' :{
//         do something
//     }
//     case 'REMOVE': {
//         do that 
//     }
//     case 'DUPLICATE': {
//         ssss
//     }
// }
import uuid from 'react-uuid';



const taskReducers = (state, action) => {
    switch(action.type) {
        case "ADD_TASK" :{
            const updated = {...action.payload, "id": uuid(), "isDone": false}
            return [...state, updated] ;
        }
        case "REMOVE_TASK": {
            const filteredTask = state.filter((task) => task.id !== action.payload);
            return [...filteredTask];
        }
        case "DONE_TASK": {
            const index = state.findIndex((task) => task.id === action.payload);
            const doneTask = [...state];
            doneTask[index].isDone = true;
            return doneTask;
        }
    }
}

const formReducers = (state, action) => {
    switch(action.type) {
        case "HANDLE_TASK": {
            return {...state, [action.key]: action.payload};
        }
    }
}


export {
    formReducers,
    taskReducers
}