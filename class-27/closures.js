// This is just an example of using throw,
// not related to closures, so ignore it unless you want to
// learn about throw
// function a() {
//     throw 'Error'
// }

// try {
//     a()
// } catch (error) {
//     console.log(error)
// }


var a = 'a'

function parent() {
    var b = 'b'

    function child() {
        var c = 'c'
        console.log(a)
        console.log(b)

        function grandChild() {
            c = "100"
            console.log(a)
            console.log(b)
            console.log(c)
        }

        return grandChild
    }

    return child
}


var childFunc = parent()
var grandChildFunc = childFunc()
grandChildFunc()



