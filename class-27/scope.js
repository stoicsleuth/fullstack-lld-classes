var a = 'a'

function parent() {
    console.log(a) // 20
    function child() {
        var a = 'a2'
    
        console.log(b) // 30
        console.log(a) // 100
        function grandChild() {
            console.log(a)
        }
        grandChild()
    }
    var b = 'b'
    child()
    console.log(a) // 100
}

parent()

// grandChild()