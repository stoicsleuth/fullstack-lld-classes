// function sum(num) {
//     return function fn1(num2) {
//         return function fn2(num3) {
//             return num + num2 + num3
//         }
//     }
// }

// Currying
const sum = (num) => (num2) => (num3) => num + num2 + num3
    


// These two are exactly equivalent
const fn1 = sum(1)
const fn2 = fn1(2)
const finalValue = fn2(3)

console.log(sum(1)(2)(3))
console.log(finalValue)



// console.log(sum(1)(2)(3)) // 6


// (sum(1))(2)
// const fn = sum1(1)
// fn(2)