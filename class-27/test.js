function parent() {
    var a = 10

    function child() {
        function grandChild() {
            a = 100

            console.log(a)
        }
        console.log(a)
        grandChild()
        console.log(a)
    }

    child()
}

parent()

function parent2() {
    var a = 10

    function child() {
        function grandChild() {
            var a = 100

            console.log(a)
        }
        console.log(a)
        grandChild()
        console.log(a)
    }

    child()
}

parent2()
