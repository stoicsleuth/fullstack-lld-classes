function parent() {
    var a = 10
    console.log(a)

    function child() {
        var b = 20
        console.log(b)
    }

    child()
}

parent()