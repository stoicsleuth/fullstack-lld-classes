// let a = [1, 2, 2, 3]
// let b = [1]

// //Length property of array -> no of items inside the array

// console.log(a.length, b.length)

// console.log(`The first element of my array is ${a[100]}`)

// // This will add an element to the end of an array
// b.push(3)
// console.log(a.length, b, b.length)

// // This will add an element to the beginning of an array
// b.unshift(10)
// console.log(a.length, b, b.length)

// // This will remove the last element of my array
// b.pop()
// console.log(a.length, b, b.length)

// // This will remove the first element of my array
// b.shift()
// console.log(a.length, b, b.length)



// Objects
// // Key-value pair where key can be a string, value can be anything, any datatype that you have seen till now

// let object = {
//   a: 5,
//   b: 10,
//   c: "hello",
//   d: null,
//   e: true,
//   f: [1, 2, 3, 4],
//   g: {
//     a: 100
//   }
// }

// console.log(object.f[2])
// console.log(object.g.a)


// // How to access the keys of an object

// // 1: Dot notation
// console.log(object.a)

// // 2: index notation
// console.log(object["c"])


// 1: function keyword
// function isEven(number) {
//   const isEvenValue = number % 2

//   // if(isEvenValue == 0) {
//   //   return true
//   // } else {
//   //   return false
//   // }

//   // Ternary operator
//   return isEvenValue == 0 ? 'True' : "False"
// }

// const returnValue = isEven(11)
// console.log(returnValue)

// const isEven = function (number) {
//   const isEvenValue = number % 2

//   // if(isEvenValue == 0) {
//   //   return true
//   // } else {
//   //   return false
//   // }

//   // Ternary operator
//   return isEvenValue == 0 ? 'True' : "False"
// }

// const returnValue = isEven(11)
// console.log(returnValue)

// Arrow function
const isEven = (number) => {
  const isEvenValue = number % 2

  // if(isEvenValue == 0) {
  //   return true
  // } else {
  //   return false
  // }

  // Ternary operator
  return isEvenValue == 0 ? 'True' : "False"
}

const returnValue = isEven(11)
console.log(returnValue)