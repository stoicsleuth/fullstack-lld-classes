// let a = 6
// let b = 9.0
// let c = -1000

// console.log(a, b, c)
// console.log(typeof a, typeof b, typeof c)

// let a = "String"
// let b = 'String'
// let c = `String`

// // Build a string like this
// // Windows 10, Windows 11, Windows X
// // Version is coming from a variable called version
// // let version = "X", Windows 10/11/X

// let version = 11
// // Template literals
// console.log(`Windows ${version}`)

// console.log(a, b, c)
// console.log(typeof a, typeof b, typeof c)

// const bool = false
// console.log(bool)
// console.log(typeof bool)


// Undefined and Null

let a;
console.log(a);

let b = null;
console.log(b);
